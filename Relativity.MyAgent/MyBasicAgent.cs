﻿namespace Relativity.MyAgent
{
    using kCura.Agent;
    
    using Relativity.API;
    using Relativity.Services.Objects;
    using Relativity.Services.Objects.DataContracts;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Net.Http;
    using System.Runtime.InteropServices;


    [kCura.Agent.CustomAttributes.Name("My Basic Sample Agent Attribute")]
    [Guid("CC4572EE-B863-47D2-8639-0EE37F1258F3")]
    public class MyBasicAgent : AgentBase
    {
        private const string ObjectComment = "Comment";

        private const string FieldArtifactID = "ArtifactID";
        private const string FieldComment = "Comment";
        private const string FieldImage = "Image";
        private const string FieldThumbnailImage = "ThumbnailImage";


        private readonly int WORKSPACE_ARTIFACTID = 1044819;

        public override string Name => "My Basic Agent Sample Name";

        public override void Execute()
        {
            string currentTime = DateTime.Now.ToLongTimeString();

            RaiseMessage($"Hello World! the time is; {currentTime}", 1);

            int workspaceId = WORKSPACE_ARTIFACTID;

            QueryResult queryResult = null;

            var queryRequest = new QueryRequest()
            {
                Condition = $"('{FieldImage}' ISSET) and (NOT('{FieldThumbnailImage}' LIKE 'base64'))",
                ObjectType = new ObjectTypeRef() { Name = ObjectComment },
                Fields = new List<FieldRef>(){
                        new FieldRef { Name= FieldArtifactID },
                        new FieldRef { Name= FieldImage },
                        new FieldRef { Name= FieldThumbnailImage }
                    },
                IncludeIDWindow = false,
            };
            try
            {
                //kCura.Relativity.Client.FileObject
                using (IObjectManager objectManager = this.Helper.GetServicesManager().CreateProxy<IObjectManager>(ExecutionIdentity.System))
                {
                    queryResult = objectManager.QueryAsync(workspaceId, queryRequest, 0, 3)?.Result;
                    if (queryResult?.ResultCount > 0)
                    {
                        foreach (var item in queryResult.Objects)
                        {
                            var thumbnailImage = item[FieldThumbnailImage]?.Value;
                            var artifactId = item[FieldArtifactID].Value as int?;
                            var tempimage = (item[FieldImage]?.Value as dynamic);

                            if (artifactId.HasValue && tempimage != null)
                            {
                                int currentartifactId = artifactId.Value;
                                UpdateManager(workspaceId, objectManager, tempimage, currentartifactId);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                RaiseWarning($"Error 1 {e.Message}", e.ToString());
            }
        }

        private void UpdateManager(int workspaceId, IObjectManager objectManager, dynamic image, int currentartifactId)
        {
            var thumbnailImageValue = this.GetThumbnailImage(currentartifactId, image.FileId, image.FileFieldArtifactId);

            try
            {
                UpdateRequest updateRequest = new UpdateRequest
                {
                    Object = new RelativityObjectRef() { ArtifactID = currentartifactId },
                    FieldValues = new List<FieldRefValuePair>
                                    {
                                       new FieldRefValuePair
                                        {
                                            Field = new FieldRef() { Name = FieldThumbnailImage },
                                            Value = thumbnailImageValue
                                        }
                                    }
                };
                var result = objectManager.UpdateAsync(workspaceId, updateRequest).Result;

            }
            catch (Exception e)
            {
                RaiseWarning($"Error on Update {e.Message}", e.ToString());
            }
        }

        private string GetThumbnailImage(int objectArtifactId, long fileId, long fileFieldArtifactId)
        {
            // Set up the client.
            string imageBase64 = "none";

            HttpClient httpClient = new HttpClient
            {
                BaseAddress = new Uri("http://localhost/")
            };

            // Set the required headers.

            httpClient.DefaultRequestHeaders.Add("X-CSRF-Header", string.Empty);
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic amVpc29udEBuc2VyaW8uY29tOk5zZXJpby4xMjM0");

            //string url = $"/Relativity/Relativity.Distributed/Download.aspx?AppID={WORKSPACE_ARTIFACTID}&ObjectArtifactID=" + objectArtifactId
            //      + $"&FileFieldArtifactID={fileFieldArtifactId}&FileID={fileId}";

            string url = $"/Relativity.REST/Workspace/{WORKSPACE_ARTIFACTID}/Comment/{objectArtifactId}/Fields/{fileFieldArtifactId}/File";

            HttpResponseMessage response = httpClient.GetAsync(url).Result;

            if (response.IsSuccessStatusCode)
            {
                try
                {
                    // Read the file.
                    Stream resultStream = response.Content.ReadAsStreamAsync().Result;

                    // The ContentDisposition header contains the name of the file name 
                    // that you are donwloading.
                    string fileName = response.Content.Headers.ContentDisposition.FileName;

                    Image image = Image.FromStream(resultStream);

                    using (Image thumbnail = image.GetThumbnailImage(50, 50, () => false, IntPtr.Zero))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            thumbnail.Save(memoryStream, ImageFormat.Png);
                            Byte[] bytes = new Byte[memoryStream.Length];
                            memoryStream.Position = 0;
                            memoryStream.Read(bytes, 0, (int)bytes.Length);
                            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                            imageBase64 = "data:image/png;base64," + base64String;
                        }
                    }
                }
                catch (Exception e)
                {
                    // An error occurred while the system was attempting to read and
                    // save the file locally.
                    // Handle the exception.
                    RaiseWarning($"Error Parsing Image{e.Message}", e.ToString());
                }
            }
            else
            {
                string resultString = response.Content.ReadAsStringAsync().Result;
                // Parse the falure result with Json.NET.
                RaiseWarning($"Error Getting Image {resultString}", resultString);

            }
            return imageBase64;
        }

    }
}
