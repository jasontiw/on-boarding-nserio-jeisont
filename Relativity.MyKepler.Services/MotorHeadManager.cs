﻿

namespace Relativity.MyKepler.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Relativity.API;
    using Relativity.MyKepler.Services.Core;
    using Relativity.MyKepler.Services.Interfaces;
    using Relativity.MyKepler.Services.Models;
    using Relativity.Services.Objects;

    public class MotorHeadManager : BaseManager, IMotorHeadManager
    {
        public MotorHeadManager(IServiceHelper _helper) : base(_helper)
        {                
        }

        public MotorHeadManager() : base(Services.Helper)
        {
        }

        public async Task<IEnumerable<string>> GetAwesomeSongLyricsAsync(string songName)
        {            
            await Task.Yield();
            var result = new[] { "jailbait", "I Lived", songName };
            return result;
        }

        public async Task<QueryResultCustom> GetCommentInfoAsync(int workspaceId, int currentArtifactId, bool getFromComment)
        {
            var bussines = new Bussines(getFromComment);
            var result = await bussines.GetCommentInfoAsync(workspaceId, currentArtifactId, Helper);

            return result;            
        }
    }
}
