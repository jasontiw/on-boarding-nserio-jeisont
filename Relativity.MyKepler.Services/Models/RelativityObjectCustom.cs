﻿namespace Relativity.MyKepler.Services.Models
{
    using Relativity.Services.Objects.DataContracts;

    public class RelativityObjectCustom : RelativityObject
    {
        public QueryResultCustom Childs { get; set; }
    }
}
