﻿namespace Relativity.MyKepler.Services.Models
{
    using Relativity.Services.Objects.DataContracts;
    using System.Collections.Generic;
    public class QueryResultCustom : QueryResult
    {
        public List<RelativityObjectCustom> CustomObjects { get; set; }
    }
}
