﻿namespace Relativity.MyKepler.Services
{
    using Relativity.API;
    using System;
    public abstract class BaseManager : IDisposable
    {
        public BaseManager(IServiceHelper Helper)
        {
            this.Helper = Helper;
        }
        protected internal IServiceHelper Helper { get; private set; }

        public void Dispose()
        {
            this.Helper.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
