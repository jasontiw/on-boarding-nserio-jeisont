﻿
namespace Relativity.MyKepler.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Relativity.Kepler.Services;
    using Relativity.MyKepler.Services.Models;

    [WebService("MotorHead")]
    [ServiceAudience(Audience.Public)]
    public interface IMotorHeadManager : IDisposable
    {
        Task<IEnumerable<string>> GetAwesomeSongLyricsAsync(string songName);

        Task<QueryResultCustom> GetCommentInfoAsync(int workspaceId, int currentArtifactId, bool getFromComment);
    }
}
