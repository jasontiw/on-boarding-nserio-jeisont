﻿
namespace Relativity.MyKepler.Services.Core
{
    using Relativity.API;
    using Relativity.MyKepler.Services.Models;
    using Relativity.Services.Objects;
    using Relativity.Services.Objects.DataContracts;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;


    public class Bussines
    {
        private const string FieldCreatedBy = "System Created By";
        private const string FieldCreatedOn = "System Created On";
        private const string FieldArtifactID = "ArtifactID";
        private const string FieldComment = "Comment";
        private const string FieldDetail = "Detail";
        private const string FieldImage = "Image";
        private const string FieldCommentDocument = "Comment Document";
        private const string FieldThumbnailImage = "ThumbnailImage";
        private const string TableComment = "Comment";

        private bool getFromComment { get; set; }

        public Bussines(bool getFromComment)
        {
            this.getFromComment = getFromComment;
        }

        public async Task<QueryResultCustom> GetCommentInfoAsync(int workspaceId,int currentArtifactId, IServiceHelper Helper)
        {
            try
            {
                QueryResultCustom queryResult = null;
                using (IObjectManager objectManager = Helper.GetServicesManager().CreateProxy<IObjectManager>(ExecutionIdentity.CurrentUser))
                {
                    queryResult = await BuildThree(objectManager, workspaceId, currentArtifactId, 0);
                }
                return queryResult;
            }
            catch (Exception)
            {
                throw;
            }
        }


        private async Task<QueryResultCustom> BuildThree(IObjectManager objectManager,
        int workspaceId, int artifactId, int numberChild)
        {
            numberChild++;

            QueryResult queryResult = null;
            try
            {
                string tempcondition = string.Empty;

                if (!getFromComment)
                {
                    tempcondition = $"('{(numberChild == 1 ? FieldCommentDocument : FieldDetail)}' == {artifactId} )";
                }
                else {
                    tempcondition = $"('{(numberChild == 1 ? FieldArtifactID : FieldDetail)}' == {artifactId} )";
                }
                
                var queryRequest = new QueryRequest()
                {
                    Condition = tempcondition,
                    ObjectType = new ObjectTypeRef() { Name = TableComment },
                    Fields = new List<FieldRef>(){
                      new FieldRef { Name= FieldArtifactID },
                      new FieldRef { Name= FieldComment },
                      new FieldRef { Name= FieldCreatedBy },
                      new FieldRef { Name= FieldCreatedOn },
                      new FieldRef { Name= FieldImage },
                      new FieldRef { Name= FieldDetail },
                      new FieldRef { Name= FieldCommentDocument },
                      new FieldRef { Name= FieldThumbnailImage }
                    },
                    MaxCharactersForLongTextValues = int.MaxValue,
                    IncludeIDWindow = false
                };
                queryResult = await objectManager.QueryAsync(workspaceId, queryRequest, 0, 10);

                if (queryResult?.ResultCount > 0)
                {
                    QueryResultCustom queryCustomResult = new QueryResultCustom
                    {
                        CustomObjects = new List<RelativityObjectCustom>()
                    };

                    queryResult.Objects.ForEach(o =>
                    {
                        queryCustomResult.CustomObjects.Add(
                            new RelativityObjectCustom()
                            {
                                ArtifactID = o.ArtifactID,
                                FieldValues = o.FieldValues,
                                Guids = o.Guids,
                                Name = o.Name,
                                ParentObject = o.ParentObject
                            }
                          );    
                    });
                    queryCustomResult.TotalCount = queryResult.TotalCount;
                    queryCustomResult.ResultCount = queryResult.ResultCount;

                    foreach (var item in queryCustomResult.CustomObjects)
                    {
                        var isNull = item[FieldArtifactID]?.Value is null;

                        if (!isNull && numberChild < 5)
                        {
                            int.TryParse(item[FieldArtifactID]?.Value.ToString(), out int currentArtifactId);

                            var result = await BuildThree(objectManager, workspaceId, currentArtifactId, numberChild);
                            if (result != null)
                            {
                                item.Childs = result;
                            }
                        }
                    }
                    return queryCustomResult;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
