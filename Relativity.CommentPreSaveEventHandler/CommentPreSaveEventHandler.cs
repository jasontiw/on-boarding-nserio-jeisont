﻿

namespace Relativity.CommentPreSaveEventHandler
{
    using kCura.EventHandler;
    using System;
    using System.Runtime.InteropServices;

    [kCura.EventHandler.CustomAttributes.Description("CommentSql Pre Save")]
    [Guid("1196FD02-5203-43E7-A818-CB41C1007038")]
    public class CommentPreSaveEventHandler : PreSaveEventHandler
    {      
        public override Response Execute()
        {
            Response retVal = new Response
            {
                Success = true,
                Message = String.Empty
            };

            try
            {
                int CreatedBy = -1;
                int workspaceId = this.Helper.GetActiveCaseID();
                
                var currentDbContext = this.Helper.GetDBContext(workspaceId);

                var currentArtifactID = currentDbContext.
                    ExecuteSqlStatementAsDbDataReader($"select CreatedBy from [EDDSDBO].Artifact WITH (NOLOCK) where ArtifactID = {this.ActiveArtifact.ArtifactID} ");

                if (currentArtifactID.HasRows)
                {
                    while (currentArtifactID.Read())
                    {
                        CreatedBy = currentArtifactID.GetInt32(0);
                    }
                }

                if (this.Helper.GetAuthenticationManager().UserInfo.ArtifactID != CreatedBy)
                {
                    retVal.Success = false;
                    retVal.Message = $"you can not modify the comment since you did not create it!";
                }
            }
            catch (Exception ex)
            {
                retVal.Success = false;
                retVal.Message = ex.ToString();
            }

            return retVal;
        }

        public override FieldCollection RequiredFields
        {
            get
            {
                FieldCollection retVal = new FieldCollection
                      {
                          new kCura.EventHandler.Field("Comment")
                      };
                return retVal;
            }
        }

        public override string Description => base.Description;

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return base.CreateObjRef(requestedType);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }



        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object InitializeLifetimeService()
        {
            return base.InitializeLifetimeService();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

    }
}
