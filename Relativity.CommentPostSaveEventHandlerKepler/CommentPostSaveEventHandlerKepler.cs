﻿
namespace Relativity.CommentPostSaveEventHandlerKepler
{
    using kCura.EventHandler;
    using Relativity.API;
    using Relativity.CommentEventHandler.Core.Services;
    using Relativity.CommentEventHandler.Core.Views;
    using Relativity.Services.Objects;
    using Relativity.Services.Objects.DataContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    [kCura.EventHandler.CustomAttributes.Description("CommentKepler Post Save")]
    [Guid("B26FDAA7-0E6C-4114-B1FF-349C3764B427")]
    public class CommentPostSaveEventHandlerKepler
     : PreSaveEventHandler
    {
        private const string FieldCreatedBy = "System Created By";
        private const string FieldArtifactID = "ArtifactID";
        private const string FieldDetail = "Detail";
        private const string FieldComment = "Comment";
        public override Response Execute()
        {
            Response retVal = new Response
            {
                Success = true,
                Message = String.Empty
            };

            try
            {               
                int workspaceId = this.Helper.GetActiveCaseID();                
                using (IObjectManager objectManager = this.Helper.GetServicesManager().CreateProxy<IObjectManager>(ExecutionIdentity.System))
                {
                    QueryResult queryResult = null;
                    var queryRequest = new QueryRequest()
                    {
                        Condition = $"('{FieldArtifactID}' == {this.ActiveArtifact.ArtifactID} )",
                        ObjectType = new ObjectTypeRef() { Name = FieldComment },
                        Fields = new List<FieldRef>(){
                            new FieldRef { Name= FieldArtifactID },
                            new FieldRef { Name= FieldCreatedBy }
                        },
                        IncludeIDWindow = false
                    };
                    queryResult = objectManager.QueryAsync(workspaceId, queryRequest, 0, 1)?.Result;
                    //is adding
                    if (queryResult?.ResultCount < 1)
                    {
                        EmailService.SendEmail(new string[] { this.Helper.GetAuthenticationManager().UserInfo.EmailAddress }, 
                            "new reaply added",
                            "you have a new reaply added",
                            new EmailSettings
                            {
                                EmailFrom = "xxxxxx@nserio.com",
                                SMTPPassword = "xxxxxx",
                                SMTPPort = 587,
                                SMTPServer = "smtp.gmail.com",
                                EnableSsl = true                                
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                retVal.Success = true;
                retVal.Message = ex.ToString();
            }
            return retVal;
        }

        public override FieldCollection RequiredFields
        {
            get
            {
                FieldCollection retVal = new FieldCollection
                      {
                          new kCura.EventHandler.Field(FieldComment),
                          new kCura.EventHandler.Field(FieldDetail)
                      };
                return retVal;
            }
        }

        public override string Description => base.Description;

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return base.CreateObjRef(requestedType);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object InitializeLifetimeService()
        {
            return base.InitializeLifetimeService();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

    }
}
