﻿namespace Relativity.CommentEventHandler.Core.Views
{
    public class EmailSettings 
    {     
        public string SMTPPassword { get; set; }
     
        public int SMTPPort { get; set; }

        public string SMTPServer { get; set; }

        public string SMTPUserName { get; set; }

        public string EmailFrom { get; set; }

        public bool EnableSsl { get; set; }

    }
}
