﻿namespace Relativity.CommentEventHandler.Core.Services
{
    using Relativity.CommentEventHandler.Core.Views;
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    public static class EmailService
    {
        public static void SendEmail(string[] emailTo, string subject, string message, EmailSettings smtpConfig)
        {
            try
            {
                if (string.IsNullOrEmpty(smtpConfig.EmailFrom)) {
                    throw new FieldAccessException("error getting EmailFrom");
                }
                if (string.IsNullOrEmpty(smtpConfig.SMTPServer))
                {
                    throw new FieldAccessException("error getting SMTPServer");
                }
                if (smtpConfig.SMTPPort == 0)
                {
                    throw new FieldAccessException("error getting SMTPPort");
                }
                MailMessage msg = new MailMessage();
                emailTo.ToList().ForEach(x => { msg.Bcc.Add(x); });
                msg.From = new MailAddress(smtpConfig.EmailFrom, "CDS Client Admin");
                msg.Subject = subject.Replace("\n", ":");
                msg.IsBodyHtml = false;
                msg.Body = message;
              
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient
                {
                    Host = smtpConfig.SMTPServer,
                    Port = smtpConfig.SMTPPort,
                    EnableSsl = smtpConfig.EnableSsl,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(smtpConfig.EmailFrom, smtpConfig.SMTPPassword)

                };

                client.EnableSsl = smtpConfig.EnableSsl;
                client.Send(msg);
                client.Dispose();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
