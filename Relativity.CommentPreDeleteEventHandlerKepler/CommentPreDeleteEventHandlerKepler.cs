﻿namespace Relativity.CommentPreDeleteEventHandlerKepler
{
    using kCura.EventHandler;
    using Relativity.API;
    using Relativity.Services.Objects;
    using Relativity.Services.Objects.DataContracts;
    using System;
    using System.Collections.Generic;
    using System.Net.Mail;
    using System.Runtime.InteropServices;

    [kCura.EventHandler.CustomAttributes.Description("CommentKepler Pre Delete")]
    [Guid("B6BD87AE-B948-4456-97B7-FFF44B85EF86")]
    public class CommentPreDeleteEventHandlerKepler : PreDeleteEventHandler
    {
        private const string NameCreatedBy = "System Created By";
        private const string NameArtifactID = "ArtifactID";
        private const string NameComment = "Comment";
        private const string errorMessage = "you can not modify the comment since you did not create it!";
        public override Response Execute()
        {
            Response retVal = new Response
            {
                Success = true,
                Message = String.Empty
            };

            try
            {
                int CreatedBy = -1;
                int workspaceId = this.Helper.GetActiveCaseID();

                QueryResult queryResult = null;

                var queryRequest = new QueryRequest()
                {
                    Condition = $"('{NameArtifactID}' == {this.ActiveArtifact.ArtifactID} )",
                    ObjectType = new ObjectTypeRef() { Name = NameComment },
                    Fields = new List<FieldRef>(){
                        new FieldRef { Name= NameArtifactID },
                        new FieldRef { Name= NameCreatedBy }
                    },
                    IncludeIDWindow = false
                };

                using (IObjectManager objectManager = this.Helper.GetServicesManager().CreateProxy<IObjectManager>(ExecutionIdentity.System))
                {
                    queryResult = objectManager.QueryAsync(workspaceId, queryRequest, 0, 1)?.Result;
                    if (queryResult?.ResultCount > 0)
                    {
                        var userId = queryResult.Objects[0]?[NameCreatedBy]?.Value as Relativity.Services.Objects.DataContracts.User;
                        CreatedBy = userId.ArtifactID;
                    }
                }
                
                if (this.Helper.GetAuthenticationManager().UserInfo.ArtifactID != CreatedBy)
                {
                    retVal.Success = false;
                    retVal.Message = errorMessage;
                    throw new InvalidOperationException(errorMessage);
                }

                using (MailMessage emailMessage = new MailMessage())
                {
                    emailMessage.From = new MailAddress("account2@gmail.com", "Account2");
                    emailMessage.To.Add(new MailAddress("account1@gmail.com", "Account1"));
                    emailMessage.Subject = "SUBJECT";
                    emailMessage.Body = "BODY";
                    emailMessage.Priority = MailPriority.Normal;
                    using (SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587))
                    {
                        MailClient.EnableSsl = true;
                        MailClient.Credentials = new System.Net.NetworkCredential("account2@gmail.com", "password");
                        MailClient.Send(emailMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                retVal.Exception = ex;
                retVal.Success = false;
                retVal.Message = ex.ToString();
            }

            return retVal;
        }


        /// <summary>
        /// Return a null object because Pre-Delete event handlers don't return any items in the Fields collection.
        /// </summary>
        public override kCura.EventHandler.FieldCollection RequiredFields
        {
            get { return null; }
        }
        public override void Commit()
        {

        }      

        public override void Rollback()
        {

        }
    }
}
