﻿namespace Relativity.CommentConsoleKepler
{
    using kCura.EventHandler;
    using kCura.Relativity.Client;
    using Relativity.Services.Objects;
    using Relativity.Services.Objects.DataContracts;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;

    [kCura.EventHandler.CustomAttributes.Description("Console Comment Kepler")]
    [Guid("85C28613-33DF-4BD3-BBFE-C197FFE91EA6")]
    public class ConsoleEventHandlerKepler : ConsoleEventHandler
    {
        private const string FieldDetail = "Detail";
        private const string FieldArtifactID = "ArtifactID";
        private const string FieldComment = "Comment";

        public override FieldCollection RequiredFields => new FieldCollection();

        public override kCura.EventHandler.Console GetConsole(PageEvent pageEvent)
        {
            int workspaceId = this.Helper.GetActiveCaseID();

            var currentHtml = new List<string>();
            List<string> tempHtml = new List<string>() { $"<p>{this.ActiveArtifact.ArtifactID}</p>" };

            var temp = BuildThree(workspaceId, this.ActiveArtifact.ArtifactID, 0);

            if (temp != null)
            {
                tempHtml.AddRange(temp);
            }

            var console = new kCura.EventHandler.Console
            {
                Title = $"Imaging Job Console {tempHtml.Count()}",
                ButtonList = new List<ConsoleButton>(),
                HTMLBlocks = tempHtml,

            };
            return console;
        }

        private List<string> BuildThree(int workspaceId, int artifactId, int numberChild)
        {
            numberChild++;

            using (IObjectManager objectManager = this.Helper.GetServicesManager().CreateProxy<IObjectManager>(API.ExecutionIdentity.System))
            {
                Services.Objects.DataContracts.QueryResult queryResult = null;
                var queryRequest = new QueryRequest()
                {
                    Condition = $"('{FieldDetail}' == {artifactId} )",
                    ObjectType = new ObjectTypeRef() { Name = FieldComment },
                    Fields = new List<FieldRef>(){
                        new FieldRef { Name= FieldArtifactID }
                    },
                    IncludeIDWindow = false
                };
                queryResult = objectManager.QueryAsync(workspaceId, queryRequest, 0, 10)?.Result;
                //is adding
                if (queryResult?.ResultCount > 0)
                {
                    List<string> tempHtml = new List<string>();

                    for (int i = 0; i < queryResult?.ResultCount; i++)
                    {
                        var isNull = queryResult.Objects[i]?[FieldArtifactID]?.Value is null;

                        if (!isNull && numberChild < 5)
                        {
                            string temppad = string.Empty;
                            for (int t = 0; t < numberChild; t++)
                            {
                                temppad = "-" + temppad;
                            }

                            int.TryParse(queryResult.Objects[i]?[FieldArtifactID]?.Value.ToString(), out int currentArtifactId);

                            tempHtml.Add("<p>" + $"{temppad}{currentArtifactId}" + "</p>");
                            var temp = BuildThree(workspaceId, currentArtifactId, numberChild);
                            if (temp != null)
                            {
                                tempHtml.AddRange(temp);
                            }
                        }
                    }
                    return tempHtml;
                }
                else
                {
                    return null;
                }
            }
        }

        public override void OnButtonClick(ConsoleButton consoleButton)
        {
            //throw new NotImplementedException();            
        }
    }
}
