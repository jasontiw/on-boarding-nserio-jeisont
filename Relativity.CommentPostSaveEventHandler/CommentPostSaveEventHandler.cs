﻿
namespace Relativity.CommentPostSaveEventHandler
{
    using kCura.EventHandler;
    using Relativity.CommentEventHandler.Core.Services;
    using Relativity.CommentEventHandler.Core.Views;
    using System;
    using System.Runtime.InteropServices;

    [kCura.EventHandler.CustomAttributes.Description("CommentSql Post Save")]
    [Guid("84EF2022-9C49-4DF8-B3B7-2830E96F8DEC")]
    public class CommentPostSaveEventHandler
    : PreSaveEventHandler
    {
        private const string FieldComment = "Comment";
        public override Response Execute()
        {
            Response retVal = new Response
            {
                Success = true,
                Message = String.Empty
            };

            try
            {
                int workspaceId = this.Helper.GetActiveCaseID();
                var currentDbContext = this.Helper.GetDBContext(workspaceId);

                var currentArtifactID = currentDbContext.
                    ExecuteSqlStatementAsDbDataReader($"select CreatedBy from [EDDSDBO].Artifact WITH (NOLOCK) where ArtifactID = {this.ActiveArtifact.ArtifactID} ");
                //is adding
                if (!currentArtifactID.HasRows)
                {
                    EmailService.SendEmail(new string[] { this.Helper.GetAuthenticationManager().UserInfo.EmailAddress },
                            "new reaply added",
                            "you have a new reaply added",
                            new EmailSettings
                            {
                                EmailFrom = "xxxxxx@nserio.com",
                                SMTPPassword = "xxxxxx",
                                SMTPPort = 587,
                                SMTPServer = "smtp.gmail.com",
                                EnableSsl = true
                            });
                }
            }
            catch (Exception ex)
            {
                retVal.Success = true;
                retVal.Message = ex.ToString();
            }

            return retVal;
        }

        public override FieldCollection RequiredFields
        {
            get
            {
                FieldCollection retVal = new FieldCollection
                      {
                          new kCura.EventHandler.Field(FieldComment)
                      };
                return retVal;
            }
        }

        public override string Description => base.Description;

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return base.CreateObjRef(requestedType);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }



        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object InitializeLifetimeService()
        {
            return base.InitializeLifetimeService();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

    }
}
