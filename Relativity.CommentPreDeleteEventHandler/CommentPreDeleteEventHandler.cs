﻿namespace Relativity.CommentPreDeleteEventHandler
{
    using kCura.EventHandler;
    using System;
    using System.Runtime.InteropServices;

    [kCura.EventHandler.CustomAttributes.Description("CommentSql Pre Delete")]
    [Guid("ADBA1948-E7FD-4B98-A433-EFF458612095")]
    public class CommentPreDeleteEventHandler: PreDeleteEventHandler
    {
        private const string errorMessage = "you can not modify the comment since you did not create it!";
        public override Response Execute()
        {
            Response retVal = new Response
            {
                Success = true,
                Message = String.Empty
            };
            int CreatedBy = -1;
            int workspaceId = this.Helper.GetActiveCaseID();
            try
            {
                var currentDbContext = this.Helper.GetDBContext(workspaceId);

                var currentArtifactID = currentDbContext.
                    ExecuteSqlStatementAsDbDataReader($"select CreatedBy from [EDDSDBO].Artifact WITH (NOLOCK) where ArtifactID = {this.ActiveArtifact.ArtifactID} ");

                if (currentArtifactID.HasRows)
                {
                    while (currentArtifactID.Read())
                    {
                        CreatedBy = currentArtifactID.GetInt32(0);
                    }
                }
                if (this.Helper.GetAuthenticationManager().UserInfo.ArtifactID != CreatedBy)
                {
                    retVal.Success = false;
                    retVal.Message = errorMessage;
                    throw new InvalidOperationException(errorMessage);
                }
            }
            catch (Exception ex)
            {
                retVal.Exception = ex;
                retVal.Success = false;
                retVal.Message = ex.ToString();
            }

            return retVal;
        }

        /// <summary>
        /// Return a null object because Pre-Delete event handlers don't return any items in the Fields collection.
        /// </summary>
        public override FieldCollection RequiredFields
        {
            get { return null; }
        }

        public override void Commit()
        {
           
        }
        

        public override void Rollback()
        {
            
        }
    }
}
