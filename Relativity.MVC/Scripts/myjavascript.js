function DeleteAllComments(currentArtifactId) {

  this.FieldCreatedBy = 'System Created By';
  this.FieldCreatedOn = 'System Created On';
  this.FieldArtifactID = 'ArtifactID';
  this.FieldComment = 'Comment';
  this.FieldImage = 'Image';
  this.FieldDetail = 'Detail';
  this.FieldCommentDocument = 'Comment Document';
  this.FieldThumbnailImage = 'ThumbnailImage';

  console.log('hello ,' + currentArtifactId);

  postData('http://192.168.0.193/Relativity.REST/api/Relativity.MyKepler.Services.Interfaces.IMotorHeadModule/MotorHead/GetCommentInfoAsync', {
      workspaceId: 1044819,
      currentArtifactId: parseInt(currentArtifactId),
      getFromComment: true
    })
    .then(data =>{
        console.log(data);
        LoopResult(data.CustomObjects);
    }    
    ) // JSON from `response.json()` call
    .catch(error => console.error(error))

  function postData(url, data) {
    // Default options are marked with *
    return fetch(url, {
        body: JSON.stringify(data), // must match 'Content-Type' header       
        headers: {
          'x-csrf-header': '',
          'authorization': 'Basic amVpc29udEBuc2VyaW8uY29tOk5zZXJpby4xMjM0',
          'content-type': 'application/json'
        },
        method: 'POST', // *GET, POST, PUT, DELETE, etc.         
      })
      .then(response => response.json()) // parses response to JSON
  }

  function LoopResult(_customObjects) {
    if (_customObjects !== null && _customObjects !== undefined) {
      _customObjects.forEach(element => {
        const indexArtifactID = (element.FieldValues.findIndex(s => s.Field.Name === this.FieldArtifactID));
        const currentComment = {};
        currentComment.ArtifactID = element.FieldValues[indexArtifactID].Value;
       
        postData('http://192.168.0.193/Relativity.REST/api/Relativity.Objects/workspace/1044819/object/delete', {
            Request:{
                Object:{
                   ArtifactID:currentComment.ArtifactID
                }
             }
          })
          .then(data =>{
            
            if (element.Childs !== undefined && element.Childs !== null) {
                LoopResult(element.Childs.CustomObjects);
             }
             else{
              window.history.back();
             }
          }    
          ) // JSON from `response.json()` call
          .catch(error => console.error(error))
      });
    }
  }

}
