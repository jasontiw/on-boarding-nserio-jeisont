namespace Relativity.MVC.Models
{
  using Relativity.Services.Objects.DataContracts;
  using System.Collections.Generic;

  public class RelativityObjectCustom : RelativityObject
  {
    public QueryResultCustom Childs { get; set; }
  }
}
