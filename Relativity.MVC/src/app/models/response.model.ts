import { Artifact } from './artifact.model';

export class ApiRestResponse {
    public TotalCount: number;
    public Objects: Objects[];
    public CustomObjects: CustomObjects[];
}
export class Objects {
    public FieldValues: FieldValues[];
}

export class CustomObjects {
    public FieldValues: FieldValues[];
    public Childs: ApiRestResponse;
}

export class FieldValues {
 public Field: Field;
 public Value;
}

export class Field extends Artifact {
    public FieldCategory: any;
    public Name: string;
}
