import { Artifact } from './artifact.model';
import { Document } from './document.model';

export class Comment extends Artifact {
    public CreatedBy: CreatedBy;
    public CreatedOn: Date;
    public Comment: string;
    public Date: Date;
    public Image: File;
    public Detail: Document;
    public CommentDocument: Document;
    public ThumbnailImage: string | null | undefined ;
    public Comments: Comment[];
}

export class CreatedBy extends Artifact {
    public EmailAddress: string;
    public Name: string;
}

export class KeyValuePair {
    public Key: string;
    public Value: any;
}

export class File {
    public FileFieldArtifactId: number;
    public FileId: number;
    public FileName: string;
}


export class CommentsByUser {
    public User: string;
    public Color: string;
    public Total: number;
}

