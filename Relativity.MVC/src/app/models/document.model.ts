import { Artifact } from './artifact.model';

export class Document extends Artifact {
    public relativityNativeType: string;
    public controlNumber: string;
    public Name: string;
}
