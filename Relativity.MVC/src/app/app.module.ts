import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { DocumentComponent } from './document/document.component';
import { DocumentService } from './document/document.component.services';
import { CommentService } from './comment/comment.component.services';

import { ModalModule } from 'ngx-bootstrap/modal';
import { CommentComponent } from './comment/comment.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToggleComponent } from './toggle/toggle.component';
import { ToggleService } from './toggle/toggle.component.services';

@NgModule({
  declarations: [
    AppComponent,
    DocumentComponent,
    CommentComponent,
    ToggleComponent
  ],
  imports: [
    FormsModule, BrowserModule, HttpClientModule, HttpModule, ModalModule.forRoot()
  ],
  providers: [DocumentService, CommentService, ToggleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
