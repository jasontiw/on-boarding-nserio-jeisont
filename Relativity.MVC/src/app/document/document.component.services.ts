import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Document } from '../models/document.model';
import { Comment, CreatedBy, File } from '../models/comment.model';
import { ApiRestResponse, CustomObjects } from '../models/response.model';
import { map } from 'rxjs/operators';

@Injectable()
export class DocumentService {
    private apiRoot: string;
    private currentGuid: string;

    private documents: Document[];
    private comments: Comment[];

    private FieldArtifactID: string;
    private FieldComment: string;
    private FieldDetail: string;
    private FieldCreatedBy: string;
    private FieldCreatedOn: string;
    private FieldImage: string;
    private FieldCommentDocument: string;
    private FieldThumbnailImage: string;

    constructor(private http: HttpClient) {
        this.apiRoot = 'http://192.168.0.193/Relativity.REST/api/Relativity.Objects/workspace/1044819/object/query';
        this.documents = [];
        this.comments = [];

        this.currentGuid = 'c755573d-da50-4a77-ba33-eabba129138b';

        this.FieldCreatedBy = 'System Created By';
        this.FieldCreatedOn = 'System Created On';
        this.FieldArtifactID = 'ArtifactID';
        this.FieldComment = 'Comment';
        this.FieldImage = 'Image';
        this.FieldDetail = 'Detail';
        this.FieldCommentDocument = 'Comment Document';
        this.FieldThumbnailImage = 'ThumbnailImage';

    }
    public getDocuments(): Document[] {

        const headers = new HttpHeaders()
            .append('X-CSRF-Header', '')
            .append('Content-Type', 'application/json')
            .append('Cache-Control', 'no-cache');

        const httpOptions = {
            headers: headers
        };

        const url = this.apiRoot;
        const bodyRequest = {
            Request: {
                ObjectType: {
                    Name: 'Document'
                },
                fields: [
                    {
                        Name: 'Control Number'
                    },
                    {
                        Name: 'Relativity Native Type'
                    },
                    {
                        Name: 'ArtifactID'
                    }
                ],
                sorts: [
                ]
            },
            start: 0,
            length: 10
        };

        this.http.post<ApiRestResponse>(url, bodyRequest, httpOptions).subscribe(
            (success) => {
                console.log(success);
                success.Objects.forEach(element => {
                    const indexControlNumber = (element.FieldValues.findIndex(s => s.Field.Name === 'Control Number'));
                    const indexNativeType = (element.FieldValues.findIndex(s => s.Field.Name === 'Relativity Native Type'));
                    const indexArtifactID = (element.FieldValues.findIndex(s => s.Field.Name === 'ArtifactID'));

                    const currentDocument = new Document();
                    currentDocument.controlNumber = element.FieldValues[indexControlNumber].Value as string;
                    currentDocument.relativityNativeType = element.FieldValues[indexNativeType].Value as string;
                    currentDocument.ArtifactID = element.FieldValues[indexArtifactID].Value as number;

                    this.documents.push(currentDocument);
                });
            },
            error => {
                console.log(error);

                const response = {
                    'TotalCount': 18353,
                    'Objects': [
                        {
                            'ParentObject': {
                                'ArtifactID': 1042098
                            },
                            'FieldValues': [
                                {
                                    'Field': {
                                        'ArtifactID': 1003667,
                                        'FieldCategory': 'Generic',
                                        'FieldType': 'FixedLengthText',
                                        'Guids': [
                                            '2a3f1212-c8ca-4fa9-ad6b-f76c97f05438'
                                        ],
                                        'Name': 'Control Number',
                                        'ViewFieldID': 1000186
                                    },
                                    'Value': '3.480169.AX1JDNNNHXUWOFGILJZNQT0DEMRXPERPB'
                                },
                                {
                                    'Field': {
                                        'ArtifactID': 1034248,
                                        'FieldCategory': 'Generic',
                                        'FieldType': 'FixedLengthText',
                                        'Guids': [
                                            'de5ca3e4-b0a5-4fd6-9792-f505c8a91866'
                                        ],
                                        'Name': 'Relativity Native Type',
                                        'ViewFieldID': 1000349
                                    },
                                    'Value': 'Outlook Express Mail Message'
                                },
                                {
                                    'Field': {
                                        'ArtifactID': 0,
                                        'FieldCategory': 'Generic',
                                        'FieldType': 'WholeNumber',
                                        'Guids': [],
                                        'Name': 'ArtifactID',
                                        'ViewFieldID': 0
                                    },
                                    'Value': 1042851
                                }
                            ],
                            'ArtifactID': 1042851,
                            'Guids': []
                        },
                        {
                            'ParentObject': {
                                'ArtifactID': 1042098
                            },
                            'FieldValues': [
                                {
                                    'Field': {
                                        'ArtifactID': 1003667,
                                        'FieldCategory': 'Generic',
                                        'FieldType': 'FixedLengthText',
                                        'Guids': [
                                            '2a3f1212-c8ca-4fa9-ad6b-f76c97f05438'
                                        ],
                                        'Name': 'Control Number',
                                        'ViewFieldID': 1000186
                                    },
                                    'Value': '3.480170.OAGD5ZSAD3GNFVNNB3D2NVFQACLP1F54B'
                                },
                                {
                                    'Field': {
                                        'ArtifactID': 1034248,
                                        'FieldCategory': 'Generic',
                                        'FieldType': 'FixedLengthText',
                                        'Guids': [
                                            'de5ca3e4-b0a5-4fd6-9792-f505c8a91866'
                                        ],
                                        'Name': 'Relativity Native Type',
                                        'ViewFieldID': 1000349
                                    },
                                    'Value': 'Outlook Express Mail Message'
                                },
                                {
                                    'Field': {
                                        'ArtifactID': 0,
                                        'FieldCategory': 'Generic',
                                        'FieldType': 'WholeNumber',
                                        'Guids': [],
                                        'Name': 'ArtifactID',
                                        'ViewFieldID': 0
                                    },
                                    'Value': 1042852
                                }
                            ],
                            'ArtifactID': 1042852,
                            'Guids': []
                        }
                    ],
                    'CurrentStartIndex': 0,
                    'ResultCount': 2,
                    'ObjectType': {
                        'ArtifactID': 1035231,
                        'Name': 'Document',
                        'Guids': [
                            '15c36703-74ea-4ff8-9dfb-ad30ece7530d'
                        ],
                        'ArtifactTypeID': 10
                    }
                };
                response.Objects.forEach(element => {
                    const indexControlNumber = (element.FieldValues.findIndex(s => s.Field.Name === 'Control Number'));
                    const indexNativeType = (element.FieldValues.findIndex(s => s.Field.Name === 'Relativity Native Type'));
                    const indexArtifactID = (element.FieldValues.findIndex(s => s.Field.Name === 'ArtifactID'));


                    const currentDocument = new Document();
                    currentDocument.controlNumber = element.FieldValues[indexControlNumber].Value as string;
                    currentDocument.relativityNativeType = element.FieldValues[indexNativeType].Value as string;
                    currentDocument.ArtifactID = element.FieldValues[indexArtifactID].Value as number;

                    this.documents.push(currentDocument);
                });
                console.log(this.documents);
            },
            () => {
                console.log('completed.');
            });
        return this.documents;
    }
    public getReplyComments(currentArtifactId: number, callback: Function): Comment[] {
        // const currentArtifactId = 1042851;

        // tslint:disable-next-line:max-line-length
        const currentUrl = `http://192.168.0.193/Relativity.REST/api/Relativity.MyKepler.Services.Interfaces.IMotorHeadModule/MotorHead/GetCommentInfoAsync`;

        const headers = new HttpHeaders()
            .append('X-CSRF-Header', '')
            .append('Content-Type', 'application/json')
            .append('Cache-Control', 'no-cache');

        const httpOptions = {
            headers: headers
        };
        const body = {
            workspaceId: 1044819,
            currentArtifactId: currentArtifactId,
            getFromComment: false,
        };
        const url = currentUrl;

        this.http.post<ApiRestResponse>(url, body, httpOptions).subscribe(
            (success) => {
                if (success) {
                    const result = this.getCurrentComments(success.CustomObjects);
                    this.comments = result;
                } else {
                    this.comments = Array<Comment>();
                }
                callback(this.comments);
            },
            error => {
                console.log(error);
                if (currentArtifactId === 1042851) {
                    const response = {
                        'CustomObjects': [{
                            'Childs': {
                                'CustomObjects': [{
                                    'Childs': null,
                                    'ParentObject': {
                                        'ArtifactID': 1003663,
                                        'Guid': null
                                    },
                                    'Name': null,
                                    'FieldValues': [{
                                        'Value': 1066018,
                                        'Field': {
                                            'FieldCategory': 0,
                                            'FieldType': 1,
                                            'ViewFieldID': 0,
                                            'ArtifactID': 0,
                                            'Guids': [],
                                            'Name': 'ArtifactID'
                                        }
                                    }, {
                                        'Value': 'd1.1.1',
                                        'Field': {
                                            'FieldCategory': 0,
                                            'FieldType': 0,
                                            'ViewFieldID': 1005372,
                                            'ArtifactID': 1063878,
                                            'Guids': [],
                                            'Name': 'Comment'
                                        }
                                    }, {
                                        'Value': {
                                            'ArtifactID': 9,
                                            'EmailAddress': 'relativity.admin@relativity.com',
                                            'Name': 'Admin, Relativity'
                                        },
                                        'Field': {
                                            'FieldCategory': 0,
                                            'FieldType': 11,
                                            'ViewFieldID': 1005367,
                                            'ArtifactID': 1063875,
                                            'Guids': [],
                                            'Name': 'System Created By'
                                        }
                                    }, {
                                        'Value': '\/Date(1528224518513)\/',
                                        'Field': {
                                            'FieldCategory': 0,
                                            'FieldType': 2,
                                            'ViewFieldID': 1005365,
                                            'ArtifactID': 1063873,
                                            'Guids': [],
                                            'Name': 'System Created On'
                                        }
                                    }],
                                    'ArtifactID': 1066018,
                                    'Guids': []
                                }],
                                'TotalCount': 0,
                                'Objects': [],
                                'IDWindow': [],
                                'CurrentStartIndex': 0,
                                'ResultCount': 0,
                                'ObjectType': null,
                                'SampleDetails': null,
                                'RankWindow': [],
                                'RelationalField': null
                            },
                            'ParentObject': {
                                'ArtifactID': 1003663,
                                'Guid': null
                            },
                            'Name': null,
                            'FieldValues': [{
                                'Value': 1063929,
                                'Field': {
                                    'FieldCategory': 0,
                                    'FieldType': 1,
                                    'ViewFieldID': 0,
                                    'ArtifactID': 0,
                                    'Guids': [],
                                    'Name': 'ArtifactID'
                                }
                            }, {
                                'Value': 'd1.1',
                                'Field': {
                                    'FieldCategory': 0,
                                    'FieldType': 0,
                                    'ViewFieldID': 1005372,
                                    'ArtifactID': 1063878,
                                    'Guids': [],
                                    'Name': 'Comment'
                                }
                            }, {
                                'Value': {
                                    'ArtifactID': 1038787,
                                    'EmailAddress': 'jeisont@nserio.com',
                                    'Name': 'Triana, Jeison'
                                },
                                'Field': {
                                    'FieldCategory': 0,
                                    'FieldType': 11,
                                    'ViewFieldID': 1005367,
                                    'ArtifactID': 1063875,
                                    'Guids': [],
                                    'Name': 'System Created By'
                                }
                            }, {
                                'Value': '\/Date(1527296029927)\/',
                                'Field': {
                                    'FieldCategory': 0,
                                    'FieldType': 2,
                                    'ViewFieldID': 1005365,
                                    'ArtifactID': 1063873,
                                    'Guids': [],
                                    'Name': 'System Created On'
                                }
                            }],
                            'ArtifactID': 1063929,
                            'Guids': []
                        }, {
                            'Childs': null,
                            'ParentObject': {
                                'ArtifactID': 1003663,
                                'Guid': null
                            },
                            'Name': null,
                            'FieldValues': [{
                                'Value': 1066019,
                                'Field': {
                                    'FieldCategory': 0,
                                    'FieldType': 1,
                                    'ViewFieldID': 0,
                                    'ArtifactID': 0,
                                    'Guids': [],
                                    'Name': 'ArtifactID'
                                }
                            }, {
                                'Value': 'd1.2',
                                'Field': {
                                    'FieldCategory': 0,
                                    'FieldType': 0,
                                    'ViewFieldID': 1005372,
                                    'ArtifactID': 1063878,
                                    'Guids': [],
                                    'Name': 'Comment'
                                }
                            }, {
                                'Value': {
                                    'ArtifactID': 9,
                                    'EmailAddress': 'relativity.admin@relativity.com',
                                    'Name': 'Admin, Relativity'
                                },
                                'Field': {
                                    'FieldCategory': 0,
                                    'FieldType': 11,
                                    'ViewFieldID': 1005367,
                                    'ArtifactID': 1063875,
                                    'Guids': [],
                                    'Name': 'System Created By'
                                }
                            }, {
                                'Value': '\/Date(1528224541407)\/',
                                'Field': {
                                    'FieldCategory': 0,
                                    'FieldType': 2,
                                    'ViewFieldID': 1005365,
                                    'ArtifactID': 1063873,
                                    'Guids': [],
                                    'Name': 'System Created On'
                                }
                            }],
                            'ArtifactID': 1066019,
                            'Guids': []
                        }],
                        'TotalCount': 0,
                        'Objects': [],
                        'IDWindow': [],
                        'CurrentStartIndex': 0,
                        'ResultCount': 0,
                        'ObjectType': null,
                        'SampleDetails': null,
                        'RankWindow': [],
                        'RelationalField': null
                    };
                    const result = this.getCurrentComments(response.CustomObjects);
                    this.comments = result;
                } else {
                    this.comments = Array<Comment>();
                }
                callback(this.comments);
            },
            () => {
                console.log('completed.');
            });

        return this.comments;
    }

    private getCurrentComments(_customObjects: CustomObjects[]): Comment[] {
        const comments = [];
        _customObjects.forEach(element => {
            const indexArtifactID = (element.FieldValues.findIndex(s => s.Field.Name === this.FieldArtifactID));
            const indexCreatedBy = (element.FieldValues.findIndex(s => s.Field.Name === this.FieldCreatedBy));
            const indexCreatedOn = (element.FieldValues.findIndex(s => s.Field.Name === this.FieldCreatedOn));
            const indexComment = (element.FieldValues.findIndex(s => s.Field.Name === this.FieldComment));
            const indexImage = (element.FieldValues.findIndex(s => s.Field.Name === this.FieldImage));
            const indexDetail = (element.FieldValues.findIndex(s => s.Field.Name === this.FieldDetail));
            const indexCommentDocument = (element.FieldValues.findIndex(s => s.Field.Name === this.FieldCommentDocument));
            const indexThumbnailImage = (element.FieldValues.findIndex(s => s.Field.Name === this.FieldThumbnailImage));


            const currentComment = new Comment();
            currentComment.ArtifactID = element.FieldValues[indexArtifactID].Value as number;
            currentComment.CreatedBy = element.FieldValues[indexCreatedBy].Value as CreatedBy;
            currentComment.CreatedOn = this.parseJsonDate(element.FieldValues[indexCreatedOn].Value as string) as Date;
            currentComment.Comment = element.FieldValues[indexComment].Value as string;
            if (indexDetail > 0) {
                currentComment.Detail = element.FieldValues[indexDetail].Value as Document;
            }
            if (indexImage > 0) {
                currentComment.Image = element.FieldValues[indexImage].Value as File;
            }
            if (indexCommentDocument > 0) {
                currentComment.CommentDocument = element.FieldValues[indexCommentDocument].Value as Document;
            }
            if (indexThumbnailImage > 0) {
                currentComment.ThumbnailImage = element.FieldValues[indexThumbnailImage].Value as string;
            }

            if (element.Childs !== undefined && element.Childs !== null) {
                currentComment.Comments = this.getCurrentComments(element.Childs.CustomObjects);
            }
            comments.push(currentComment);
        });
        return comments;
    }

    private parseJsonDate(jsonDateString: string) {
        if (jsonDateString.indexOf('T') > 0) {
            return new Date(jsonDateString);
        }
        return new Date(parseInt(jsonDateString.replace('/Date(', ''), 0));
    }

    public dailyForecast() {
        return this.http.get('http://samples.openweathermap.org/data/2.5/history/city?q=Warren,OH&appid=b6907d289e10d714a6e88b30761fae22')
            .pipe(
                map(result => {
                    return result;
                })
            );
    }

    public GetUserByComments() {
        const headers = new HttpHeaders()
            .append('X-CSRF-Header', '')
            .append('Content-Type', 'application/json')
            .append('Cache-Control', 'no-cache');

        const httpOptions = {
            headers: headers
        };
        const body = {
            Request: {
               ObjectType: {
                  Name: 'Comment'
               },
               fields: [
                   {
                     Name: 'System Created By'
                  }, {
                     Name: 'ArtifactID'
                  }
               ],
               sorts: [
               ],
            },
            start: 0,
            length: 0
         };

        return this.http.post<ApiRestResponse>('http://192.168.0.193/Relativity.REST/api/Relativity.Objects/workspace/1044819/object/query',
            body, httpOptions)
            .pipe(
                map(result => {
                    return result;
                })
            );
    }
}

