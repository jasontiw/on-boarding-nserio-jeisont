import { Component, TemplateRef, OnInit } from '@angular/core';
import { DocumentService } from './document.component.services';
import { Document } from '../models/document.model';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import * as $ from 'jquery';
import { Comment, CreatedBy, CommentsByUser } from '../models/comment.model';

import { Chart } from 'chart.js';
import { ApiRestResponse } from '../models/response.model';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {
  modalRef: BsModalRef;
  public name: string;
  public documents: Document[];
  private currentDocument: Document;
  public comments: Comment[];
  public chart = [];

  private currentTheme: boolean;

  private commentsByUser: Array<CommentsByUser>;

  private _documentService: DocumentService;

  constructor(_documentService: DocumentService,
    private modalService: BsModalService) {
    this.name = 'document';
    this._documentService = _documentService;
    this.documents = _documentService.getDocuments();

    this.commentsByUser = Array<CommentsByUser>();
  }

  ngOnInit() {
    const self = this;
    self._documentService.GetUserByComments()
      .subscribe(res => {
        self.BuildChartJsByApiResult(res);

      });
  }

  private BuildChartJsByApiResult(response: ApiRestResponse) {
    const self = this;

    if (response !== null && (response.TotalCount !== null && response.TotalCount !== -1)) {

      response.Objects.forEach(item => {
        const indexCreatedBy = (item.FieldValues.findIndex(s => s.Field.Name === 'System Created By'));
        const createdBy = item.FieldValues[indexCreatedBy].Value as CreatedBy;

        const indexUserContains = self.commentsByUser.findIndex(s => s.User === createdBy.Name);
        if (indexUserContains > -1) {
          self.commentsByUser[indexUserContains].Total += 1;
        } else {
          const tempComment = new CommentsByUser();
          tempComment.User = createdBy.Name;
          tempComment.Total = 1;
          self.commentsByUser.push(
            tempComment
          );
        }
      });

      this.chart = new Chart('canvas', {
        type: 'pie',
        data: {
          labels: self.commentsByUser.map(a => a.User),
          datasets: [{
            backgroundColor: [
              '#2ecc71',
              '#3498db',
              '#95a5a6',
              '#9b59b6',
              '#f1c40f',
              '#e74c3c',
              '#34495e'
            ],
            data: self.commentsByUser.map(a => a.Total)
          }]
        }
      });
    }
  }

  public openModal(template: TemplateRef<any>, event, document: Document) {
    this.getComments(document);
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }
  private getComments(document: Document) {
    const self = this;
    self.currentDocument = document;
    this.comments = this._documentService.getReplyComments(self.currentDocument.ArtifactID, function (call) {
      self.comments = call;
    });
  }

  /**
   *
   *
   * @param {*} story
   * @memberof DocumentComponent
   */
  public reloadComments(story: any) {
    const self = this;
    this.getComments(self.currentDocument);
  }

  /**
   *
   *
   * @param {boolean} theme
   * @memberof DocumentComponent
   */
  public onChangeToggle(theme: boolean) {
    this.currentTheme = theme;
  }
}
