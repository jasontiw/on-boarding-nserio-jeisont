import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ToggleService {
    private apiRoot: string;
    private currentGuid: string;

    constructor(private http: HttpClient) {
        this.apiRoot = 'http://192.168.0.193/Relativity.REST/api/Relativity.Objects/workspace/1044819/object/create';
        this.currentGuid = 'c755573d-da50-4a77-ba33-eabba129138b';
    }

    public GetCurrentTheme(): boolean {
        const currentTheme = localStorage.currentTheme;
        if (currentTheme === undefined) {
            localStorage.currentTheme = true;
        }
        return localStorage.currentTheme === 'true' ? true : false ;
    }

    public UpdateCurrentTheme(): boolean {
        const currentTheme = this.GetCurrentTheme();
        localStorage.currentTheme = !currentTheme;
        return !currentTheme;
    }

}
