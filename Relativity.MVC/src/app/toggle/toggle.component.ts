import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ToggleService } from './toggle.component.services';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.css']
})
export class ToggleComponent implements OnInit {
  private currentToggle: boolean;
  private toggleService: ToggleService;
  @Output() changeToggle = new EventEmitter<boolean>();

  constructor(_toggleService: ToggleService) {
    this.toggleService = _toggleService;
   }

  ngOnInit() {
    this.currentToggle = this.toggleService.GetCurrentTheme();
    this.changeToggle.emit(this.currentToggle);
  }

  public onChangeToggle() {
    this.toggleService.UpdateCurrentTheme();
    this.changeToggle.emit(this.currentToggle);
  }

}
