import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Comment } from '../models/comment.model';
import { CommentService } from './comment.component.services';
import { Document } from '../models/document.model';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() comments: Array<Comment>;
  @Input() showFirstForm: boolean;
  @Input() currentArtifactID: number;

  @Output() reloadComments = new EventEmitter<boolean>();

  private showReply: Array<boolean>;
  private newcomment: string;
  private _commentService: CommentService;

  private _showFirstForm: boolean;
  private _DomSanitizer: DomSanitizer;

  constructor(_commentService: CommentService, _DomSanitizer: DomSanitizer) {
    this._commentService = _commentService;
    this._DomSanitizer = _DomSanitizer;
  }

  ngOnInit() {
    this.buildInit();
    if (this.showFirstForm === true) {
      this._showFirstForm = this.showFirstForm;
    }

  }

  private buildInit() {
    if (this.comments.length) {
      this.showReply = new Array<boolean>(this.comments.length);
      for (let index = 0; index < this.comments.length; index++) {
        this.showReply[index] = false;
      }
    }
  }

  public clickShowReply(i: number) {
    if (this.showReply === undefined) {
      this.buildInit();
    }
    if (this.showReply[i] !== undefined) {
      this.showReply[i] = !this.showReply[i];
    }
    if (this.showFirstForm === true) {
      this._showFirstForm = !this._showFirstForm;
    }
  }
  public clickSubmitReply(currentComment: Comment, i: number, newComment: boolean | null) {
    const self = this;

    if (currentComment === null) {
      currentComment = new Comment();
      currentComment.CommentDocument = new Document();
      currentComment.CommentDocument.ArtifactID = this.currentArtifactID;
    }

    currentComment.Comment = self.newcomment;
    currentComment.Date = new Date();

    if (newComment === undefined) {
      if (currentComment.Detail === null || currentComment.Detail === undefined) {
        currentComment.Detail = new Document();
      }
      currentComment.Detail.ArtifactID = currentComment.ArtifactID;
      currentComment.CommentDocument = null;
    }

    this._commentService.addReplyComments(currentComment, function (call) {
      if (currentComment.Comments === undefined) {
        currentComment.Comments = Array<Comment>();
      }

      self.newcomment = '';
      if (newComment === undefined) {
        self.clickShowReply(i);
      }
      self.reloadComments.emit(true);
    });
  }

  public reloadCommentsMethod(story) {
    const self = this;
    self.reloadComments.emit(true);
  }


  // At the drag drop area
  // (drop)="onDropFile($event)"
  public onDropFile(event: DragEvent, comment: Comment) {
    event.preventDefault();
    this.uploadFile(event.dataTransfer.files, comment);
  }

  // At the drag drop area
  // (dragover)="onDragOverFile($event)"
  public onDragOverFile(event) {
    event.stopPropagation();
    event.preventDefault();
  }

  // At the file input element
  // (change)="selectFile($event)"
  public selectFile(event, comment: Comment) {
    this.uploadFile(event.target.files, comment);
  }

  public uploadFile(files: FileList, comment: Comment) {
    if (files.length === 0) {
      console.log('No file selected!');
      return;

    }
    const self = this;
    const file: File = files[0];

    const dataAsFormData = new FormData();
    dataAsFormData.append(file.name, file);
    dataAsFormData.append('FileFieldArtifactId', '1063916'); // fieldid
    dataAsFormData.append('FileContainerInstanceArtifactId', comment.ArtifactID.toString());
    this._commentService.addImage(dataAsFormData, function() {
      self.reloadComments.emit(true);
    });
  }

  public GetDownloadURL(comment: Comment): Object {
    if (comment.Image !== undefined && comment.Image !== null) {
      return {
        Url:
          // tslint:disable-next-line:max-line-length
          'http://192.168.0.193/Relativity.Distributed/Download.aspx?AppID=1044819&ObjectArtifactID=' + comment.ArtifactID + '&FileFieldArtifactID=1063916&FileID=' + comment.Image.FileId.toString()
        , Target: '_new'
      };
    } else {
      return {
        Url: '#'
        , Target: ''
      };
    }
  }
}
