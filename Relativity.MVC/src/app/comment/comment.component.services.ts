import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Document } from '../models/document.model';
import { Comment, CreatedBy, File } from '../models/comment.model';
import { ApiRestResponseCreate, Object } from '../models/create_response.model';


@Injectable()
export class CommentService {
    private apiRoot: string;
    private currentGuid: string;

    private documents: Document[];
    private comments: Comment[];

    private FieldArtifactID: string;
    private FieldComment: string;
    private FieldDetail: string;
    private FieldDate: string;
    private FieldCreatedBy: string;
    private FieldCreatedOn: string;
    private FieldImage: string;
    private FieldCommentDocument: string;

    constructor(private http: HttpClient) {
        this.apiRoot = 'http://192.168.0.193/Relativity.REST/api/Relativity.Objects/workspace/1044819/object/create';
        this.documents = [];
        this.comments = [];

        this.currentGuid = 'c755573d-da50-4a77-ba33-eabba129138b';

        this.FieldCreatedBy = 'System Created By';
        this.FieldCreatedOn = 'System Created On';
        this.FieldArtifactID = 'ArtifactID';
        this.FieldComment = 'Comment';
        this.FieldDetail = 'Detail';
        this.FieldDate = 'Date';
        this.FieldImage = 'Image';
        this.FieldCommentDocument = 'Comment Document';

    }
    public addReplyComments(newComment: Comment, callback: Function): Comment[] {
        const currentUrl = this.apiRoot;
        const headers = new HttpHeaders()
            .append('X-CSRF-Header', '')
            .append('Content-Type', 'application/json')
            .append('Cache-Control', 'no-cache');

        const httpOptions = {
            headers: headers
        };

        const url = currentUrl;
        const bodyRequest = {
            Request: {
                ObjectType: {
                    Name: 'Comment'
                },
                FieldValues: [
                    {
                        Field: { Name: this.FieldComment },
                        Value: newComment.Comment
                    },
                    {
                        Field: { Name: this.FieldDetail},
                        Value: newComment.Detail
                    },
                    {
                        Field: { Name: this.FieldDate},
                        Value: newComment.Date
                    },
                    {
                        Field: { Name: this.FieldCommentDocument},
                        Value: newComment.CommentDocument
                    }
                ]
            }
        };
        this.http.post<ApiRestResponseCreate>(url, bodyRequest, httpOptions).subscribe(
            (success) => {
                this.comments = Array<Comment>();
                if (success) {
                    const result = this.getCurrentComments(success.Object);
                    this.comments = result;
                }
                callback(this.comments);
            },
            error => {
                this.comments = Array<Comment>();
                console.log(error);
                this.comments.push(newComment);
                callback(this.comments);
            },
            () => {
                console.log('completed.');
            });

        return this.comments;
    }

    private getCurrentComments(_customObjects: Object): Comment[] {
        const comments = [];
        const indexArtifactID = (_customObjects.FieldValues.findIndex(s => s.Field.Name === this.FieldArtifactID));
        const indexCreatedBy = (_customObjects.FieldValues.findIndex(s => s.Field.Name === this.FieldCreatedBy));
        const indexCreatedOn = (_customObjects.FieldValues.findIndex(s => s.Field.Name === this.FieldCreatedOn));
        const indexComment = (_customObjects.FieldValues.findIndex(s => s.Field.Name === this.FieldComment));
        const indexImage = (_customObjects.FieldValues.findIndex(s => s.Field.Name === this.FieldImage));
        const indexDetail = (_customObjects.FieldValues.findIndex(s => s.Field.Name === this.FieldDetail));
        const indexCommentDocument = (_customObjects.FieldValues.findIndex(s => s.Field.Name === this.FieldCommentDocument));

        const currentComment = new Comment();
        if (indexArtifactID > -1) {
            currentComment.ArtifactID = _customObjects.FieldValues[indexArtifactID].Value as number;
        }
        if (indexCreatedBy > -1) {
            currentComment.CreatedBy = _customObjects.FieldValues[indexCreatedBy].Value as CreatedBy;
        }
        if (indexCreatedOn > -1) {
            currentComment.CreatedOn = this.parseJsonDate(_customObjects.FieldValues[indexCreatedOn].Value as string) as Date;
        }
        if (indexComment > -1) {
            currentComment.Comment = _customObjects.FieldValues[indexComment].Value as string;
        }
        if (indexDetail > -1) {
            currentComment.Detail = _customObjects.FieldValues[indexDetail].Value as Document;
        }
        if (indexImage > -1) {
            currentComment.Image = _customObjects.FieldValues[indexImage].Value as File;
        }
        if (indexCommentDocument > -1) {
            currentComment.CommentDocument = _customObjects.FieldValues[indexCommentDocument].Value as Document;
        }

        comments.push(currentComment);
        return comments;
    }

    private parseJsonDate(jsonDateString: string) {
        if (jsonDateString.indexOf('T') > 0) {
            return new Date(jsonDateString);
        }
        return new Date(parseInt(jsonDateString.replace('/Date(', ''), 0));
    }


    public addImage(body: FormData, callback: Function): Comment[] {

        const headers = new HttpHeaders()
            .append('X-CSRF-Header', '')
            .append('Cache-Control', 'no-cache');

        const httpOptions = {
            headers: headers
        };

        const currentUrl = `http://192.168.0.193/Relativity/CustomPages/${this.currentGuid}/Home/UploadImageFile/`;

        const url = currentUrl;
        this.http.post<ApiRestResponseCreate>(url, body, httpOptions).subscribe(
            (success) => {
                console.log(success);
                callback();
            },
            error => {
                console.log(error);
                callback();
            },
            () => {
                console.log('completed.');
            });

        return this.comments;
    }
}

