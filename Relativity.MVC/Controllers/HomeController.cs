using kCura.Relativity.Client;
using Relativity.API;
using Relativity.MVC.Models;
using Relativity.Services.Objects;
using Relativity.Services.Objects.DataContracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Relativity.MVC.Controllers
{
  public class HomeController : Controller
  {
    private const string FieldCreatedBy = "System Created By";
    private const string FieldCreatedOn = "System Created On";
    private const string FieldArtifactID = "ArtifactID";
    private const string FieldComment = "Comment";
    private const string FieldDetail = "Detail";
    private const string FieldImage = "Image";
    private const string FieldCommentDocument = "Comment Document";
    private const string FieldThumbnailImage = "ThumbnailImage";


    private const string TableComment = "Comment";

    // GET: Home
    public ActionResult Index()
    {
      try
      {
        //Get the user's first and last name with Relativity API Helpers
        String firstName = Relativity.CustomPages.ConnectionHelper.Helper().GetAuthenticationManager().UserInfo.FirstName;
        String lastName = Relativity.CustomPages.ConnectionHelper.Helper().GetAuthenticationManager().UserInfo.LastName;

        //Get the current time
        String currentTime = DateTime.Now.ToLongTimeString();

        //Set the text value        
        String helloStr = String.Format("Hello {0} {1}! The current time is: {2}",
                    firstName,
                    lastName,
                    currentTime);

        //Set message
        ViewBag.Message = helloStr;
      }
      catch (Exception Ex)
      {
        Console.WriteLine(Ex.Message);
      }

      return View();
    }

    public async Task<ActionResult> GetCommentInfoAsync(int currentArtifactId)
    {
      try
      {
        int workspaceId = CustomPages.ConnectionHelper.Helper().GetActiveCaseID();
        QueryResultCustom queryResult = null;
        using (IObjectManager objectManager = CustomPages.ConnectionHelper.Helper().GetServicesManager().CreateProxy<IObjectManager>(ExecutionIdentity.CurrentUser))
        {
          queryResult = await BuildThree(objectManager, workspaceId, currentArtifactId, 0);
        }

        return Json(queryResult, JsonRequestBehavior.AllowGet);
      }
      catch (Exception)
      {
        throw;
      }
    }

    private async Task<QueryResultCustom> BuildThree(IObjectManager objectManager,
      int workspaceId, int artifactId, int numberChild)
    {
      numberChild++;

      Services.Objects.DataContracts.QueryResult queryResult = null;
      try
      {
        var queryRequest = new QueryRequest()
        {
          Condition = $"('{(numberChild == 1 ? FieldCommentDocument : FieldDetail)}' == {artifactId} )",
          ObjectType = new ObjectTypeRef() { Name = TableComment },
          Fields = new List<FieldRef>(){
          new FieldRef { Name= FieldArtifactID },
          new FieldRef { Name= FieldComment },
          new FieldRef { Name= FieldCreatedBy },
          new FieldRef { Name= FieldCreatedOn },
          new FieldRef { Name= FieldImage },
          new FieldRef { Name= FieldDetail },
          new FieldRef { Name= FieldCommentDocument },
          new FieldRef { Name= FieldThumbnailImage }
        },
          IncludeIDWindow = false
        };
        queryResult = await objectManager.QueryAsync(workspaceId, queryRequest, 0, 10);

        if (queryResult?.ResultCount > 0)
        {
          QueryResultCustom queryCustomResult = new QueryResultCustom
          {
            CustomObjects = new List<RelativityObjectCustom>()
          };

          queryResult.Objects.ForEach(o =>
          {
            queryCustomResult.CustomObjects.Add(
                new RelativityObjectCustom()
                {
                  ArtifactID = o.ArtifactID,
                  FieldValues = o.FieldValues,
                  Guids = o.Guids,
                  Name = o.Name,
                  ParentObject = o.ParentObject
                }
              );
          });
          queryCustomResult.TotalCount = queryResult.TotalCount;
          queryCustomResult.ResultCount = queryResult.ResultCount;

          foreach (var item in queryCustomResult.CustomObjects)
          {
            var isNull = item[FieldArtifactID]?.Value is null;

            if (!isNull && numberChild < 5)
            {
              int.TryParse(item[FieldArtifactID]?.Value.ToString(), out int currentArtifactId);

              var result = await BuildThree(objectManager, workspaceId, currentArtifactId, numberChild);
              if (result != null)
              {
                item.Childs = result;
              }
            }
          }
          return queryCustomResult;
        }
        else
        {
          return null;
        }
      }
      catch (Exception)
      {

        throw;
      }

    }


    public int UpdateTheme(int workSpaceId, bool value)
    {
      int data;

      using (IRSAPIClient proxy = CustomPages.ConnectionHelper.Helper().GetServicesManager().CreateProxy<IRSAPIClient>(ExecutionIdentity.System))
      {
        int workspace = workSpaceId;
        IDBContext dbContext = CustomPages.ConnectionHelper.Helper().GetDBContext(-1);
        string sql = $@"UPDATE [eddsdbo].[InstanceSetting]
                          SET [Value] = '{value}'
                          WHERE 
                          [Name] LIKE '%Theme UI (light/dark)%'";
        data = dbContext.ExecuteNonQuerySQLStatement(sql);
      }
      return data;
    }


    public Theme GetTheme(int workSpaceId)
    {
      DataRowCollection data;
      Theme theme = new Theme();
      using (IRSAPIClient proxy = CustomPages.ConnectionHelper.Helper().GetServicesManager().CreateProxy<IRSAPIClient>(ExecutionIdentity.System))
      {
        int workspace = workSpaceId;
        IDBContext dbContext = CustomPages.ConnectionHelper.Helper().GetDBContext(-1);
        string sql = $@"SELECT [Value] FROM [eddsdbo].[InstanceSetting]
                              WHERE Name LIKE '%Theme UI (light/dark)%'";
        data = dbContext.ExecuteSqlStatementAsDataTable(sql).Rows;
        foreach (DataRow item in data)
        {
          foreach (var d in item.ItemArray)
          {
            theme.textValue = (string)d;
          }
        }
        theme.value = theme.textValue.Equals("true") ? true : false;
        theme.textValue = theme.value ? "LIGHT" : "DARK";
      }

      return theme;
    }

    public bool UploadImageFile(UploadModel model)
    {
      int workspaceId = CustomPages.ConnectionHelper.Helper().GetActiveCaseID();
      string filepath = "~/Images";

      if (model.FileFieldArtifactId == 0)
      {
        return false;
      }
      if (model.FileContainerInstanceArtifactId == 0)
      {
        return false;
      }
      if (Request.Files != null && Request.Files.Count > -1)
      {
        try
        {
          foreach (string file in Request.Files)
          {
            var fileContent = Request.Files[file];
            if (fileContent != null && fileContent.ContentLength > 0)
            {
              var inputStream = fileContent.InputStream;
              var fileName = Path.GetFileName(file);
              var path = Path.Combine(Server.MapPath(filepath), fileName);
              using (var fileStream = System.IO.File.Create(path))
              {
                inputStream.CopyTo(fileStream);
              }
              using (IRSAPIClient proxy = CustomPages.ConnectionHelper.Helper().GetServicesManager().CreateProxy<IRSAPIClient>(ExecutionIdentity.System))
              {
                var currentAPIoptions = proxy.APIOptions;
                currentAPIoptions.WorkspaceID = workspaceId;

                var uploadRequest = new UploadRequest(currentAPIoptions);
                uploadRequest.Metadata.FileName = path;
                uploadRequest.Metadata.FileSize = new FileInfo(uploadRequest.Metadata.FileName).Length;

                uploadRequest.Overwrite = true;
                uploadRequest.Target.FieldId = model.FileFieldArtifactId;
                uploadRequest.Target.ObjectArtifactId = model.FileContainerInstanceArtifactId;

                proxy.Upload(uploadRequest);
              }
            }
          }
        }
        catch (Exception)
        {
          return false;
        }
        return true;
      }
      {
        return false;
      }
    }


  }


  public class UploadModel
  {
    public string Title { get; set; }
    public string Description { get; set; }
    public HttpPostedFileBase Attachment { get; set; }

    public int FileFieldArtifactId { get; set; }
    public int FileContainerInstanceArtifactId { get; set; }
  }

}
