﻿namespace Relativity.CommentPreSaveEventHandlerKepler
{
    using kCura.EventHandler;
    using Relativity.API;
    using Relativity.Services.Objects;
    using Relativity.Services.Objects.DataContracts;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    [kCura.EventHandler.CustomAttributes.Description("CommentKepler Pre Save")]
    [Guid("A47FD700-8DF3-4FF9-A100-4A30CF4FE494")]
    public class CommentPreSaveEventHandlerKepler : PreSaveEventHandler
    {
        private const string NameCreatedBy = "System Created By";
        private const string NameArtifactID = "ArtifactID";
        private const string NameComment = "Comment";
        public override Response Execute()
        {
            Response retVal = new Response
            {
                Success = true,
                Message = String.Empty
            };

            try
            {
                int CreatedBy = -1;
                int workspaceId = this.Helper.GetActiveCaseID();

                QueryResult queryResult = null;

                var queryRequest = new QueryRequest()
                {
                    Condition = $"('{NameArtifactID}' == {this.ActiveArtifact.ArtifactID} )",
                    ObjectType = new ObjectTypeRef() { Name = NameComment },
                    Fields = new List<FieldRef>(){
                        new FieldRef { Name= NameArtifactID },
                        new FieldRef { Name= NameCreatedBy }
                    },
                    IncludeIDWindow = false
                };

                using (IObjectManager objectManager = this.Helper.GetServicesManager().CreateProxy<IObjectManager>(ExecutionIdentity.System))
                {
                    queryResult = objectManager.QueryAsync(workspaceId, queryRequest, 0, 1)?.Result;
                    if (queryResult?.ResultCount > 0)
                    {
                        var userId = queryResult.Objects[0]?[NameCreatedBy]?.Value as Relativity.Services.Objects.DataContracts.User;
                        CreatedBy = userId.ArtifactID;
                    }
                }              
                if (this.Helper.GetAuthenticationManager().UserInfo.ArtifactID != CreatedBy)
                {
                    retVal.Success = false;
                    retVal.Message = $"you can not modify the comment since you did not create it!";
                }
            }
            catch (Exception ex)
            {
                retVal.Success = false;
                retVal.Message = ex.ToString();
            }

            return retVal;
        }

        public override FieldCollection RequiredFields
        {
            get
            {
                FieldCollection retVal = new FieldCollection
                      {
                          new kCura.EventHandler.Field("Comment")
                      };
                return retVal;
            }
        }

        public override string Description => base.Description;

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return base.CreateObjRef(requestedType);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object InitializeLifetimeService()
        {
            return base.InitializeLifetimeService();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

    }
}
