﻿

namespace Relativity.CommentConsole
{
    using kCura.EventHandler;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;

    [kCura.EventHandler.CustomAttributes.Description("Console Comment")]
    [Guid("DF8B7465-6446-42ED-A7FD-DC7D44D2970F")]
    public class CommentConsoleEventHandler : ConsoleEventHandler
    {
        public override FieldCollection RequiredFields => new FieldCollection();


        public override kCura.EventHandler.Console GetConsole(PageEvent pageEvent)
        {

            int workspaceId = this.Helper.GetActiveCaseID();

            var currentDbContext = this.Helper.GetDBContext(workspaceId);
            var currentHtml = new List<string>();
            List<string> tempHtml = new List<string>() { $"<p>{this.ActiveArtifact.ArtifactID}</p>" };
            
            var temp = BuildThree(currentDbContext, this.ActiveArtifact.ArtifactID, 0);

            if (temp != null)
            {
                tempHtml.AddRange(temp);
            }
           
            var console = new kCura.EventHandler.Console
            {
                Title = $"Imaging Job Console {tempHtml.Count()}",
                ButtonList = new List<ConsoleButton>(),
                HTMLBlocks = tempHtml,                
                
            };
            return console;
        }

        private List<string> BuildThree(API.IDBContext currentDbContext, int artifactId, int numberChild)
        {
            numberChild++;

            var currentArtifactID = currentDbContext.
                ExecuteSqlStatementAsDataTable($"select Detail,ArtifactID from [EDDSDBO].Comment WITH (NOLOCK) where Detail = {artifactId} ");

            if (currentArtifactID.Rows.Count >0)
            {
                List<string> tempHtml = new List<string>();
               
                for (int i = 0; i < currentArtifactID.Rows.Count; i++)
                {
                    var colIndex = currentArtifactID.Rows[0];//currentArtifactID.GetOrdinal("Detail");
                    var isNull = currentArtifactID.Rows[i].IsNull(0); //currentArtifactID.IsDBNull(colIndex);

                    //throw new InvalidOperationException("error" + isNull.ToString() + " - " + currentArtifactID.GetInt32(0).ToString());
                    if (!isNull && numberChild<5)
                    {
                        string temppad = string.Empty;
                        for (int t = 0; t < numberChild; t++)
                        {
                            temppad = "-"+ temppad;
                        }

                        int.TryParse(currentArtifactID.Rows[i]["ArtifactID"].ToString(), out int currentArtifactId);

                        tempHtml.Add("<p>" + $"{temppad}{currentArtifactId}" + "</p>");
                        var temp = BuildThree(currentDbContext, currentArtifactId, numberChild);
                        if (temp != null)
                        {
                            tempHtml.AddRange(temp);
                        }
                    }
                }
                return tempHtml;
            }
            else {
                return null;
            }            
        }

        public override void OnButtonClick(ConsoleButton consoleButton)
        {
            //throw new NotImplementedException();            
        }
    }
}
