﻿namespace Relativity.CommentPageInteraction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;

    [kCura.EventHandler.CustomAttributes.Description("This is a sample page interaction event handler")]
    [Guid("6AA86C89-D02A-46A8-AB69-7D14E08C0DEA")]
    public class SamplePageInteractionEventHandler : kCura.EventHandler.PageInteractionEventHandler
    {
        
        public override kCura.EventHandler.Response PopulateScriptBlocks()
        {
            //Create a response object with default values.
            kCura.EventHandler.Response retVal = new kCura.EventHandler.Response
            {
                Success = true,
                Message = String.Empty
            };

            //Retreive the URL to the custom pages to add the JavaScript and CSS.
            //  monona String applicationPath = getApplicationPath(this.Application.ApplicationUrl);

            // Before the elements are loaded on a page, register the JavaScript file. 
            // Load a JavaScript file into Relativity via a custom page.
            //  monona this.RegisterLinkedClientScript(applicationPath + "javascript/myjavascriptfunctions.js");

            // Register functions directly.
            String alertFunction = "<script type=\"text/javascript\"> function alertWithText(text){alert(text);}</script>";
            this.RegisterClientScriptBlock(new kCura.EventHandler.ScriptBlock() { Key = "alertFunc", Script = alertFunction });
            var currentArtifactId = this.ActiveArtifact.ArtifactID;
            

            string myscript = "<script type=\"text/javascript\">" +
                "document.addEventListener('DOMContentLoaded', function(){ " +
                "var node = document.createElement('a'); " +
                "node.classList = 'ActionButton';" +
                "node.text = 'Delete All Comments';" +
                $"node.onclick = function() {{ DeleteAllComments('{currentArtifactId}'); }};" +
                "document.getElementsByClassName('centerDynamicTemplateCell')[0].appendChild(node)" +
                "}, false);" +
                "</script>";
            this.RegisterClientScriptBlock(new kCura.EventHandler.ScriptBlock() { Key = "alertFunc2", Script = myscript });


            string applicationPath = this.Helper.GetUrlHelper().GetRelativePathToCustomPages(new Guid("c755573d-da50-4a77-ba33-eabba129138b"));
            this.RegisterLinkedStartupScript(applicationPath + "Scripts/myjavascript.js");


            // After the elements are loaded on the page, register the JavaScript.
            //  monona  this.RegisterLinkedStartupScript(applicationPath + "functionCall.js");

            // Call the functions directly.
            //String alert = "<script type=\"text/javascript\"> alertWithText('Successfully called a function registered earlier!');</script>";
            //this.RegisterStartupScriptBlock(new kCura.EventHandler.ScriptBlock() { Key = "alertKey", Script = alert });

            // The custom page can include a .css file for loading into a page.
            //  monona  this.RegisterLinkedCss(applicationPath + "styles/loadedCSS.css");
            return retVal;
        }
    }
}
